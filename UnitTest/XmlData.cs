﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using NUnit.Framework;

namespace UnitTest
{
	public class XmlData
	{
		public static IEnumerable Data
		{
			get
			{
				List<Credentials> data = new List<Credentials>();
				data.Add(new Credentials { Login = "epamTT@tut.by", Pass = "12345qwer", LoginRecipient = "epamTT2@tut.by" });
				data.Add(new Credentials { Login = "epamTT2@tut.by", Pass = "12345qwer", LoginRecipient = "epamTT@tut.by" });

				// передаем в конструктор тип класса
				XmlSerializer formatter = new XmlSerializer(typeof(List<Credentials>));

				// получаем поток, куда будем записывать сериализованный объект
				using (FileStream fs = new FileStream("creds.xml", FileMode.OpenOrCreate))
				{
					formatter.Serialize(fs, data);

					Console.WriteLine("Объект сериализован");
				}

				List<Credentials> creds;
				// десериализация
				using (FileStream fs = new FileStream("creds.xml", FileMode.OpenOrCreate))
				{
					creds = (List<Credentials>)formatter.Deserialize(fs);

					Console.WriteLine("Имя: {0} --- Возраст: {1}", creds[0].Login, creds[0].Pass);
				}
				foreach (var d in creds)
				{
					yield return new TestCaseData(d.Login, d.Pass, d.LoginRecipient);
				}
			}
		}
	}
}
