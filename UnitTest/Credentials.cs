﻿namespace UnitTest
{
	public class Credentials
	{
		public string Login { get; set; }
		public string Pass { get; set; }
		public string LoginRecipient { get; set; }
	}
}
