﻿using System;
using System.Drawing.Imaging;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace UnitTest
{
	[TestFixture]
	public class TutByTests
	{
		private ChromeDriver chrome;

		[SetUp]
		public void Setup()
		{
			if (chrome == null)
				chrome = new ChromeDriver();
		}

		[TearDown]
		public void TearDown()
		{
			chrome.Quit();
			chrome.Dispose();
			chrome = null;
		}

		[Test]
		[TestCaseSource(typeof(DbSqlite), "Creds")]
		[TestCaseSource(typeof(XmlData), "Data")]
		[TestCaseSource(typeof(CsvData), "Creds")]
		public void TutByLoginAndSend(string login, string pass, string recipientLogin)
		{
			try
			{
				chrome.Navigate().GoToUrl("http://mail.tut.by/");
				//Login
				Login(login, pass);
				//Create new letter
				Thread.Sleep(3000);
				chrome.FindElement(By.CssSelector("[class='b-ico b-ico_compose']")).Click();
				//Fill subject and text
				Thread.Sleep(2000);

				var uid = DateTime.Now.Ticks.ToString();
				chrome.FindElement(By.Id("nb-23")).SendKeys(recipientLogin);
				chrome.FindElement(By.Name("subj")).SendKeys($"TestLetterFromEpamTT: {uid}");

				chrome.FindElement(By.Id("compose-send_ifr")).SendKeys("Letter body is here");
				chrome.FindElement(By.Id("compose-submit")).Click();

				//Go to sent
				Thread.Sleep(3000);
				chrome.FindElement(
					By.XPath("//div[2]/div/div[5]/div/div[2]/div[2]/div/div/div[1]/div[1]/div/div[2]/span[2]/a")).Click();
				Thread.Sleep(3000);

				//Find desired uid
				FindTextOnPage(uid);

				//Logout
				chrome.FindElement(By.CssSelector("span.header-user-pic.b-mail-dropdown__handle")).Click();
				chrome.FindElement(By.XPath("//div[5]/div/div/div/div[9]/a")).Click();
				Thread.Sleep(3000);

				chrome.Navigate().GoToUrl("http://mail.tut.by/");
				Thread.Sleep(3000);

				Login(recipientLogin, pass);
				Thread.Sleep(3000);

				FindTextOnPage(uid);
			}
			catch (Exception ex)
			{
				MakeScreenShot(chrome.GetScreenshot());
				Assert.Fail(ex.Message);
			}
		}

		private void Login(string login, string pass)
		{
			chrome.FindElement(By.Id("Username")).SendKeys(login);
			chrome.FindElement(By.Id("Password")).SendKeys(pass);
			chrome.FindElement(By.Id("Password")).SendKeys(Keys.Enter);
		}

		private void FindTextOnPage(string text)
		{
			string bodyText = chrome.FindElement(By.TagName("body")).Text;
			Assert.IsTrue(bodyText.Contains(text));
		}

		private void MakeScreenShot(Screenshot scr)
		{
			scr.SaveAsFile($"{AppDomain.CurrentDomain.BaseDirectory}//test.png", ImageFormat.Png);
		}
	}
}
