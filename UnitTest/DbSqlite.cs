﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace UnitTest
{
	public class DbSqlite
	{
		public static IEnumerable Creds
		{
			get
			{
				List<Credentials> data = new List<Credentials>();
				var filename = $"{AppDomain.CurrentDomain.BaseDirectory}//MyDatabase.sqlite";
				var mDbConnection = new SQLiteConnection($"Data Source={filename};Version=3;");
				try
				{
					SQLiteCommand command;
					string sql;
					if (!File.Exists(filename))
					{
						SQLiteConnection.CreateFile(filename);
						mDbConnection.Open();

						sql = "create table creds (login varchar(20), pass varchar(20), login2 varchar(20))";
						command = new SQLiteCommand(sql, mDbConnection);
						command.ExecuteNonQuery();

						sql = "insert into creds (login, pass, login2) values ('epamTT@tut.by', '12345qwer', 'epamTT2@tut.by')";
						command = new SQLiteCommand(sql, mDbConnection);
						command.ExecuteNonQuery();
						sql = "insert into creds (login, pass, login2) values ('epamTT2@tut.by', '12345qwer', 'epamTT@tut.by')";
						command = new SQLiteCommand(sql, mDbConnection);
						command.ExecuteNonQuery();
					}

					if (mDbConnection.State != ConnectionState.Open)
						mDbConnection.Open();
					sql = "select * from creds";
					command = new SQLiteCommand(sql, mDbConnection);
					SQLiteDataReader reader = command.ExecuteReader();

					while (reader.Read())
					{
						data.Add(new Credentials
						{
							Login = reader["login"].ToString(),
							Pass = reader["pass"].ToString(),
							LoginRecipient = reader["login2"].ToString()
						});
					}

				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
				}
				finally
				{
					mDbConnection.Close();
				}

				return data.Select(d => new TestCaseData(
					d.Login, d.Pass, d.LoginRecipient));
			}
		}
	}
}
