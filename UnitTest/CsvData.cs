﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using NUnit.Framework;

namespace UnitTest
{
	public class CsvData
	{
		public static IEnumerable Creds
		{
			get
			{
				List<Credentials> creds = new List<Credentials>();
				using (var reader = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}//creds.csv"))
				{
					while (!reader.EndOfStream)
					{
						var splits = reader.ReadLine().Split(';');
						creds.Add(new Credentials { Login = splits[0], Pass = splits[1], LoginRecipient = splits[2] });
					}
				}
				foreach (var d in creds)
				{
					yield return new TestCaseData(d.Login, d.Pass, d.LoginRecipient);
				}
			}
		}
	}
}
